# Lazy Loading To Avoid Pagination (Vanilla JS)
### Task 2 - Front End Developer - NextGrowth Labs
## Overview

In this project, we will implement lazy loading using Vanilla JavaScript to avoid traditional pagination. Lazy loading ensures a smooth user experience by automatically loading more results as the user scrolls down the page. We'll achieve this without relying on any external libraries or plugins. Instead, we'll generate random data to simulate an infinite scrolling experience.

**Note:** Please ensure that you only use publicly available APIs for this project.

## Project Objectives

- Implement lazy loading to load additional results when the user reaches the end of the current results.
- Simulate an infinite scrolling experience.
- Create a user-friendly interface to showcase the lazy loading feature.
- Achieve these objectives using Vanilla JavaScript without external libraries or frameworks.
